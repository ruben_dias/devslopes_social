//
//  Post.swift
//  devslopes-social
//
//  Created by Ruben Dias on 07/01/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import Foundation
import Firebase

class Post {
    private var _caption: String!
    private var _imageURL: String!
    private var _likes: Int!
    private var _postKey: String!
    private var _postRef: DatabaseReference!
    
    var caption: String {
        return _caption
    }
    
    var imageURL: String {
        return _imageURL
    }
    
    var likes: Int {
        return _likes
    }
    
    var postKey: String {
        return _postKey
    }
    
    init(caption: String, imageURL: String, likes: Int) {
        self._caption = caption
        self._imageURL = imageURL
        self._likes = likes
    }
    
    init(postKey: String, postData: Dictionary<String, AnyObject>) {
        self._postKey = postKey
        
        if let caption = postData["caption"] as? String {
            self._caption = caption
        }
        
        if let imageURL = postData["imageURL"] as? String {
            self._imageURL = imageURL
        }
        
        if let likes = postData["likes"] as? Int {
            self._likes = likes
        }
        
        _postRef = DataService.ds.REF_POSTS.child(_postKey)
    }
    
    func updatePost(data: Dictionary<String, AnyObject>) -> Bool {
        var shouldReload = false
        let caption = data["caption"] as! String
        let likes = data["likes"] as! Int
        
        if (self._caption != caption) {
            self._caption = caption
            shouldReload = true
        }
        if(self._likes != likes) {
            self._likes = likes
            shouldReload = true
        }
        return shouldReload
    }
    
    func adjustLikes(addLike: Bool) {
        if(addLike) {
            _postRef.child("likes").setValue(_likes + 1)
        } else {
            _postRef.child("likes").setValue(_likes - 1)
        }
    }
}
