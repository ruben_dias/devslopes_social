//
//  EnhancedTextField.swift
//  devslopes-social
//
//  Created by Ruben Dias on 05/01/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import UIKit

class EnhancedTextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderColor = UIColor(red: SHADOW_GRAY, green: SHADOW_GRAY, blue: SHADOW_GRAY, alpha: 0.2).cgColor
        layer.borderWidth = 1.0
        layer.cornerRadius = 3.0
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y + 2, width: bounds.width - 25, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y + 2, width: bounds.width - 25, height: bounds.height)
    }
}
