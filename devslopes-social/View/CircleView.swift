//
//  CircleView.swift
//  devslopes-social
//
//  Created by Ruben Dias on 05/01/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import UIKit

class CircleView: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = self.frame.width / 2
    }
}
