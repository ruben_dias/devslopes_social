//
//  EnhancedView.swift
//  devslopes-social
//
//  Created by Ruben Dias on 05/01/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import UIKit

class EnhancedView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.shadowColor = UIColor(red: SHADOW_GRAY, green: SHADOW_GRAY, blue: SHADOW_GRAY, alpha: 0.4).cgColor
        layer.shadowOpacity = 0.6
        layer.shadowRadius = 1
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        
        layer.cornerRadius = 3.0
    }
}
