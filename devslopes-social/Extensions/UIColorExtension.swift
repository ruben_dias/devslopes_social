//
//  UIColorExtension.swift
//  devslopes-social
//
//  Created by Ruben Dias on 07/01/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}
