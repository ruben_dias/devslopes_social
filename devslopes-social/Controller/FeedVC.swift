//
//  FeedVC.swift
//  devslopes-social
//
//  Created by Ruben Dias on 05/01/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import UIKit
import Photos
import SwiftKeychainWrapper
import Firebase
import CoreGraphics

class FeedVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addImage: RoundedView!
    @IBOutlet weak var captionField: EnhancedTextField!
    
    var posts = [Post]()
    var imagePicker: UIImagePickerController!
    static var imageCache: NSCache<NSString, UIImage> = NSCache()
    var imageSelected = false
    
    var insertingImage: Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        // hide white bounce space
        dealWithTableViewBounce()
        
        DataService.ds.REF_POSTS.observe(.value) { (snapshot) in
            let contentOffset = self.tableView.contentOffset
            var newPost = false
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    if let postDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        
                        // update specific post, otherwise add it
                        if let index = self.posts.index(where: {$0.postKey == key}) {
                            let originalPost = self.posts[index]
                            if (originalPost.updatePost(data: postDict)) {
                                self.tableView.beginUpdates()
                                let indexPath = NSIndexPath(row: index, section: 0)
                                self.tableView.reloadRows(at: [indexPath as IndexPath], with: UITableViewRowAnimation.none)
                                self.tableView.endUpdates()
                                print("RUBEN: updated post \(key)")
                            }
                        } else {
                            newPost = true
                            let post = Post(postKey: key, postData: postDict)
                            self.posts.insert(post, at: 0)
                            self.tableView.beginUpdates()
                            self.tableView.insertRows(at: [NSIndexPath(row: 0, section: 0) as IndexPath], with: .top)
                            self.tableView.endUpdates()
                        }
                    }
                }
            }
            if(newPost) {
                let start = CGPoint(x: 0, y: 0)
                self.tableView.setContentOffset(start, animated: true)
            } else {
                self.tableView.setContentOffset(contentOffset, animated: true)
            }
        }
    }
    
    // TABLE VIEW CODE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = posts[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell") as? PostCell {
            if let image = FeedVC.imageCache.object(forKey: post.imageURL as NSString) {
                cell.configureCell(post: post, img: image)
            } else {
                cell.configureCell(post: post)
            }
            
            return cell
        } else {
            return PostCell()
        }
    }
    
    
    // IMAGE PICKER CODE
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            addImage.backgroundColor = UIColor.white
            addImage.image = image
            imageSelected = true
        } else {
            print("RUBEN: a valid image was not selected.")
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    // ACTIONS
    
    @IBAction func signOutTapped(_ sender: Any) {
        KeychainWrapper.standard.removeObject(forKey: KEY_UID)
        
        try! Auth.auth().signOut()
        performSegue(withIdentifier: "goToSignIn", sender: nil)
    }
    
    @IBAction func addImageTapped(_ sender: Any) {
        let status = PHPhotoLibrary.authorizationStatus()
        
        if(status == PHAuthorizationStatus.authorized) {
            present(imagePicker, animated: true, completion: nil)
        } else {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized {
                    self.present(self.imagePicker, animated: true, completion: nil)
                } else {
                    // TODO: PRESENT SOME USER ERROR MESSAGE
                }
            })
        }
    }
    
    @IBAction func textFieldEntered(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func postButtonTapped(_ sender: Any) {
        guard let caption = captionField.text, caption != "" else {
            print("RUBEN: a caption must be entered.")
            return
        }
        guard let image = addImage.image, imageSelected == true else {
            print("RUBEN: an image must be selected.")
            return
        }
        
        if(!insertingImage) {
            insertingImage = true
            view.endEditing(true)
            
            if let imageData = UIImageJPEGRepresentation(image, 0.2) {
                let imageUID = NSUUID().uuidString
                let metadata = StorageMetadata()
                metadata.contentType = "image/jpeg"
                
                DataService.ds.REF_POST_IMAGES.child(imageUID).putData(imageData, metadata: metadata) { (metadata, error) in
                    if error != nil {
                        print("RUBEN: error uploading the image to Firebase Storage.")
                    } else {
                        print("RUBEN: successfully uploaded image to Firebase Storage.")
                        if let downloadURL = metadata?.downloadURL()?.absoluteString {
                            self.postToFirebase(imgURL: downloadURL)
                        }
                    }
                    self.insertingImage = false
                }
            }
        }
    }
    
    func postToFirebase(imgURL: String) {
        let post: Dictionary<String, Any> = [
            "caption": captionField.text! as String,
            "imageURL": imgURL as String,
            "likes": 0 as Int
        ]
        
        let firebasePost = DataService.ds.REF_POSTS.childByAutoId()
        firebasePost.setValue(post)
        
        captionField.text = ""
        imageSelected = false
        addImage.backgroundColor = UIColor.init(red: 0, green: 150, blue: 136)
        addImage.image = UIImage(named: "add-image")
    }
    
    func dealWithTableViewBounce() {
        let bgView = UIView()
        bgView.backgroundColor = UIColor(red: 245, green: 245, blue: 245)
        self.tableView.backgroundView = bgView
    }
}
