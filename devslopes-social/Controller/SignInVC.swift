//
//  ViewController.swift
//  devslopes-social
//
//  Created by Ruben Dias on 04/01/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftKeychainWrapper

class SignInVC: UIViewController {
    
    @IBOutlet weak var emailField: EnhancedTextField!
    @IBOutlet weak var passwordField: EnhancedTextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let _ = KeychainWrapper.standard.string(forKey: KEY_UID) {
            performSegue(withIdentifier: "goToFeed", sender: nil)
        }
    }
    
    @IBAction func facebookButtonPressed(_ sender: Any) {
        let facebookLogin = FBSDKLoginManager()
        
        facebookLogin.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if error != nil {
                print("RUBEN: unable to authenticate with Facebook. (\(String(describing: error)))")
            } else if result?.isCancelled == true {
                print("RUBEN: user cancelled Facebook authentication.")
            } else {
                print("RUBEN: successfully authenticated with Facebook.")
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                self.firebaseAuth(credential: credential)
            }
        }
    }
    
    @IBAction func emailSignInTapped(_ sender: Any) {
        view.endEditing(true)
        if let email = emailField.text, let password = passwordField.text {
            Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                if error == nil {
                    if let user = user {
                        print("RUBEN: email user authenticated with Firebase.")
                        let userData = ["provider": user.providerID]
                        self.completeSignIn(uid: user.uid, userData: userData)
                    }
                } else {
                    Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
                        if error != nil {
                            print("RUBEN: unable to authenticate with Firebase using email.")
                        } else {
                            if let user = user {
                                print("RUBEN: email user authenticated with Firebase.")
                                let userData = ["provider": user.providerID]
                                self.completeSignIn(uid: user.uid, userData: userData)
                            }
                        }
                    })
                }
            })
        }
    }
    
    @IBAction func userEnteredEmail(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func userEnteredPassword(_ sender: Any) {
        if emailField.text != nil, emailField.text != "" {
            emailSignInTapped(self)
        } else {
            view.endEditing(true)
        }
    }
    
    func firebaseAuth(credential: AuthCredential) {
        Auth.auth().signIn(with: credential) { (user, error) in
            if error != nil {
                print("RUBEN: unable to authenticate with Firebase. (\(String(describing: error)))")
            } else {
                if let user = user {
                    print("RUBEN: authenticated with Firebase.")
                    let userData = ["provider": credential.provider]
                    self.completeSignIn(uid: user.uid, userData: userData)
                }
            }
        }
    }
    
    func completeSignIn(uid: String, userData: Dictionary<String, String>) {
        DataService.ds.createFirebaseDBUser(uid: uid, userData: userData)
        
        let keychainResult = KeychainWrapper.standard.set(uid, forKey: KEY_UID)
        print("RUBEN: user data saved to Keychain (\(keychainResult)).")
        performSegue(withIdentifier: "goToFeed", sender: nil)
    }
}

