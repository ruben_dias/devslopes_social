//
//  Constants.swift
//  devslopes-social
//
//  Created by Ruben Dias on 05/01/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import UIKit

let SHADOW_GRAY: CGFloat = 120.0 / 255.0

let KEY_UID = "uid"
